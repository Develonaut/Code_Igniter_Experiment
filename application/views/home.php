<main class="flex">
  <section class="content flex">
    <div class="welcome flex">
      <div class="landing-logo icn flex-item"></div>
      <div class="landing-text flex-item">
        <h1 class="welcome-message">
          <span>Welcome To The</span>
          <span>Exiled Order</span>
        </h1>
      <a class="register-button button" href="http://exiledorder.com/forums/index.php?/register/"><span class="button-text">REGISTER</span></a></div>
    </div>
  </section>
  </main>
  <div class="video-cover">
    <div class="video-cover-inner hide-iframe" data-role="video_cover_inner">
      <div class="top"></div>
      <div class= "bg-video" id="ytplayer" data-role="video"></div>
      <div class= "bg-image"></div>
    </div>
  </div>