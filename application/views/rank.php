<div style="height: 100px;"></div>
<center>
<img src="images/rank_banner.png" /><br/>
<a onClick="return toggleMe('qa1')" href="javascript:void(0)"><img src="images/lead_bar.jpg" /></a>
<a onClick="return toggleMe('qa2')" href="javascript:void(0)"><img src="images/admin_bar.jpg" /></a>
<a onClick="return toggleMe('qa3')" href="javascript:void(0)"><img src="images/legal_bar.jpg" /></a>
<a onClick="return toggleMe('qa4')" href="javascript:void(0)"><img src="images/log_bar.jpg" /></a>
<a onClick="return toggleMe('qa5')" href="javascript:void(0)"><img src="images/game_bar.jpg" /></a>
</center>
<br/><br/>
<center>
<div id="answerSection" style="width : 1000px;; text-align : left;">
<div id="qa1" style="font-size : 14px;"> 
    &nbsp;<img src="images/founder.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>Unfathomable</li>
   </ul>
   <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>Only given out to three (3) of the original leaders/founders of Exiled Order, this rank is unfathomable.</li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>Level 10</li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/leader.png" /><br/>
   <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Responsible for the creation, direction and execution of the community's vision, mission, and values through establishing the community's key priorities to ensure all administrative and 
   targeted improvements work toward furthering Exiled Order’s long term goals.
   </li>
   <li>
	Oversees the Community Development and Security on a Daily, Monthly, Quarterly and Annual basis. Such operations include oversight of Operations and Justice department infrastructures, 
	policies and procedures, and ensuring that community needs are addressed to ensure the best possible experience for members. Along with calculating financial risks, planning and 
	record-keeping for invoices to ensure proper performance on a daily basis.
   </li>
   <li>
   Ensures that all departments are stable and that future aspects are provided with the required resources needed to ensure growth and stability.
   </li>
   <li>
   Has the authority to abolish any rules, policies, processes or entities, as long as it has received a Two/Thirds majority between all of the Leadership.
   </li>
   <li>
   Possesses the authority to evict any member with administrative power of any sort from their department and/or sector for reasons of corruption, abuse of power, abuse of members, abuse of 
   resources, lack of activity, state of emergency or for any other reason deemed inappropriate by an administrative board.
   </li>
   <li>
   Must approve all structural changes within the Community, i.e: the Chain of Command.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Requires a total of three (3) years in Exiled Order.
   </li>
   <li>
   Must not have any active warning points and/or punishments in the past two (2) years and six (6) months.
   </li>
   <li>
   Must be dedicated to all aspects and membership of Exiled Order, along with being able to successfully lead and oversee all operations of the community.
   </li>
   <li>
   Must be selected and approved by 2/3 of the Leaders.
   </li>
   <li>
   Must be at least 21 years of age. 
   </li>   
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 9
   </li>
   </ul>
   
</div>

<div id="qa2"> 
	&nbsp;<img src="images/head_admin.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Provides assistance in maintaining the direction of the community’s vision, mission, and values. Also ensures that all departments are stable and that future aspects are provided with the required resources needed 
   to ensure growth and stability.
   </li>
   <li>
   Required to maintain connections, communication, and coordination amongst all of the departments and gaming sections.
   </li>
   <li>
   Must aid the Leadership in the critical thinking and decision making processes and must be a part of the approval process to all structural changes within the Community, i.e: the Chain of Command.
   </li>
   <li>
   Other responsibilities include but are not limited to: Personnel Management, Community Development, Security and Finance. 
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Has been in the community for at least two year(s) to be nominated for the position.
   </li>
   <li>
   Must be approved by the active the Leadership.
   </li>
   <li>
   Must maintain a clean disciplinary record.
   </li>
   <li>
   Must be at least 21 years of age. 
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 8
   </li>
   </ul> 
   <hr size="1px;" color="#ba9c22"><br/>
    &nbsp;<img src="images/admin.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Provides assistance in maintaining the direction of the community’s vision, mission, and values. Also ensures that all departments are stable and that future aspects are provided with the required resources needed 
   to ensure growth and stability.
   </li>
   <li>
   Required to maintain connections, communication, and coordination amongst all of the departments and gaming sections.
   </li>
   <li>
   Addresses and manages member concerns that escalate through the Chain of Command.
   </li>
   <li>
   Other responsibilities include but are not limited to: Personnel Management and Community Development. 
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Has been in the community for at least one year(s) and 10 months to be nominated for the position.
   </li>
   <li>
   Must be approved by the active the Leadership.
   </li>
   <li>
   Must maintain a clean disciplinary record.
   </li>
   <li>
   Must be at least 21 years of age. 
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 8
   </li>
   </ul>
</div>
<div id="qa3"> 
	&nbsp;<img src="images/judge_advo.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Oversees the department and encourages them to enforce the rules, regulations and policies.
   </li>
   <li>
   Create and manage rules, regulations and policies.
   </li>
   <li>
   Provide legality, limitations and advice to all current, future and past sponsorships; Being able to set up contracts for said sponsorships.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be able to successfully lead a Gaming Section and/or Department.
   </li>
   <li>
   Must be nominated/selected from the Operation Admins along with the Leadership.
   </li>
   <li>
   Must be at least 21 years of age.
   </li>
   <li>
   Requires a total of One Year and 3 Months in Exiled Order.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 7
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/marshal.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Ensures that no rules, regulations and policies are being broken, Marshals are the “Police Chiefs/Moderators” of the community.
   </li>
   <li>
   Handles all incoming player reports.
   </li>
   <li>
   Handles all ban appeals.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be dedicated to all aspects and membership of Exiled Order.
   </li>
   <li>
   Must be nominated/selected from the Administration Staff.
   </li>
   <li>
   Requires a total of One Year in Exiled Order.
   </li>
   <li>
   Must be at least 20 years of age.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 6
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/mil_police.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Ensures that no rules, regulations and policies are being broken, MP’s are the “Police/Moderators” of the community.
   </li>
   <li>
   Handles all incoming player reports and escalates any ban appeals to the Marshal.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be dedicated to all aspects and membership of Exiled Order.
   </li>
   <li>
   Must be nominated/selected from the Administration Staff.
   </li>
   <li>
   Must maintain a clean disciplinary record.
   </li>
   <li>
   Requires the Rank of Member 2nd Grade.
   </li>
   <li>
   Must be at least 20 years of age.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 6
   </li>
   </ul>
   
   
</div>
<div id="qa4">
	&nbsp;<img src="images/op_foreman.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Oversees the structure, direction, and sustained progress of the department.
   </li>
   <li>
   Ensures that all Operational personnel are carrying out their duties timely, effectively, and in a professional manner.
   </li>
   <li>
   Deals with any high-level issues and concerns that may arise and acts as a liaison between Logistics, Operations and  Leadership.
   </li>
   <li>
   Responsible for the continuous upkeep of the finances, objectives, and operations of the department.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be able to successfully lead a Gaming Section and/or Department.
   </li>
   <li>
   Must be nominated/selected from the Administrators, along with the Leadership.
   </li>
   <li>
   Requires a total of One Year in Exiled Order. 
   </li>
   <li>
   Must be at least 21 years of age.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 7
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/engineer.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Responsible for carrying out detailed assessments of games, designs and all other potential or proposed developments.
   </li>
   <li>
   Responds to suggestions from community members and thoroughly evaluates the practicality and value of each one without bias.
   </li>
   <li>
   Develop well founded and valuable ideas, supported through research, in order to improve the community.
   </li>
   <li>
   Provides technical support to community members.
   </li>
   <li>
   Ensuring the maintenance of all community servers for all past, present and future.
   </li>
   <li>
   Assists all other community staff in the development of their respective divisions and the community as a whole. 
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be dedicated to all aspects and membership of Exiled Order.
   </li>
   <li>
   Must be nominated/selected from the Administration Staff and Foreman.
   </li>
   <li>
   Requires a total of 10 months in Exiled Order.
   </li>
   <li>
   Must be at least 20 years of age.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 6
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/architect.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Responsible for carrying out detailed assessments of games, designs and all other potential or proposed developments.
   </li>
   <li>
   Responds to suggestions from community members and thoroughly evaluates the practicality and value of each one without bias.
   </li>
   <li>
   Develop well founded and valuable ideas, supported through research, in order to improve the community.
   </li>
   <li>
   Provides technical support to community members.
   </li>
   <li>
   Assists all other community staff in the development of their respective divisions and the community as a whole. 
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be dedicated to all aspects and membership of Exiled Order.
   </li>
   <li>
   Requires a total of 8 Months in Exiled Order.
   </li>
   <li>
   Must be at least 20 years of age.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 6
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
    &nbsp;<img src="images/publisher.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Maintains the community YouTube and Twitch.tv channels.
   </li>
   <li>
   Also responsible in creating video content and entertainment to produce recruitment via our gaming channels.
   </li>
   <li>
   Creates graphic designs for both the membership of the community and the community as a whole.
   </li>
   <li>
   Handles all social media and advertising within the community.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must maintain a clean disciplinary record.
   </li>
   <li>
   Requires a total of 8 Months in Exiled Order.
   </li>
   <li>
   Must be at least 20 years of age.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 6
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
    &nbsp;<img src="images/Secretary.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Processes all applications for membership within the community.
   </li>
   <li>
   Also handles all transfer, LOA, and discharge requests.
   </li>
   <li>
   Maintains of the community roster.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must maintain a clean disciplinary record.
   </li>
   <li>
   Requires the rank of Member 2nd Grade.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 6
   </li>
   </ul>
</div>
<div id="qa5"> 
	&nbsp;<img src="images/game_leader.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Provides assistance in maintaining the direction of the community’s vision, mission, and values. Also ensures that all departments are stable and that future aspects are provided with the required resources needed 
   to ensure growth and stability.
   </li>
   <li>
   Required to maintain connections, communication, and coordination amongst all of the departments and gaming sections.
   </li>
   <li>
   Addresses and manages member concerns that escalate through the Chain of Command.
   </li>
   <li>
   Other responsibilities include but are not limited to: Personnel Management and Community Development.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be dedicated to all aspects and membership of Exiled Order and be able to successfully lead a Gaming Section.
   </li>
   <li>
   Must be nominated/selected from the Administrators, along with the Leadership.
   </li>
   <li>
   Must be at least 21 years of age.
   </li>
   <li>
   Requires a total of One Year and 6 Months in Exiled Order.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 7
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/game_co_leader.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Ensures and promotes methods of growth and stability within their designated gaming section.
   </li>
   <li>
   Provides administrative support to assigned games while working with other members of the interdisciplinary team to ensure a stable and clean community environment.
   </li>
   <li>
   Interact in the daily processes and decisions that are provided by each function.
   </li>
   <li>
   Addresses and manages member concerns that escalate throughout the Chain of Command of the community.
   </li>
   <li>
   Promote activity and organization (including events) in and out of game.
   </li>
   <li>
   Ensures the promotion of upholding the laws and policies of Exiled Order.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must be dedicated to all aspects and membership of Exiled Order.
   </li>
   <li>
   Must be nominated/selected from the Administration Staff.
   </li>
   <li>
   Requires a total of One Year in Exiled Order.
   </li>
   <li>
   Must be at least 20 years of age.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 6
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
    &nbsp;<img src="images/sen_member_2.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Ensures and promotes methods of growth and stability within their designated gaming section.
   </li>
   <li>
   Interact in the daily processes and decisions that are provided by each function.
   </li>
   <li>
   Addresses and manages member concerns that escalate throughout the Chain of Command of the community.
   </li>
   <li>
   Promote activity and organization (including events) in and out of game.
   </li>
   <li>
   Ensures the promotion of upholding the laws and policies of Exiled Order.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must continue to remain active on forums, game servers, and voice servers.
   </li>
   <li>
   Requires a total of 10 Months in Exiled Order.
   </li>
   <li>
   Must not have been sanctioned in the last eight (8) months.
   </li>
   <li>
   Must be nominated/selected from the ranks above.
   </li>
   <li>
   Must be dedicated to all aspects and membership of their respective game section.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 5
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/sen_member_1.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Provide input and feedback on discussions that impact the direction of the game section that they are apart of.
   </li>
   <li>
   Ensures that all members of the game section are focused and performing in the roles and responsibilities of their rank.
   </li>
   <li>
   Provide vouchers for potential Prospects that are in the registration queue for the section.
   </li>
   <li>
   Organize recruitment methods and hold members of their game section.
   </li>
   <li>
   Must accountable for their actions, along with reporting any violations of policy and procedure to proper channels.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must continue to remain active on forums, game servers, and voice servers.
   </li>
   <li>
   Requires a total of 8 Months in Exiled Order.
   </li>
   <li>
   Must not have been sanctioned in the last six (6) months.
   </li>
   <li>
   Must be nominated/selected from the ranks above.
   </li>
   <li>
   Must be dedicated to all aspects and membership of their respective game section.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 5
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/mem_2_grade.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Actively recruits new prospects on any of our public gaming servers.
   </li>
   <li>
   Provide input and feedback on discussions that impact the direction of the game section that they are apart of.
   </li>
   <li>
   Provide hands on participation and support in all department activities.
   </li>
   <li>
   Provide leadership support and direction on the forums, voice servers, and in game.
   </li>
   <li>
   Helps organize recruitment methods and hold members of their game section.
   </li>
   <li>
   As well as to report any violations of policy and procedure to proper channels.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must continue to remain active on forums, game servers, and voice servers.
   </li>
   <li>
   Requires a total of 6 Months in Exiled Order.
   </li>
   <li>
   Must not have been sanctioned in the last four (4) months.
   </li>
   <li>
   Must be nominated/selected from the ranks above.
   </li>
   <li>
   Must be dedicated to all aspects and membership of their respective game section.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 4
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/mem_1_grade.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Actively recruits new prospects on any of our public gaming servers.
   </li>
   <li>
   Provide input and feedback on discussions that impact the direction of the game section that they are apart of.
   </li>
   <li>
   Helps organize recruitment methods and hold members of their game section.
   </li>
   <li>
   As well as to report any violations of policy and procedure to proper channels.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must remain active on forums, game servers, and voice servers.
   </li>
   <li>
   Requires a total of 4 Months in Exiled Order.
   </li>
   <li>
   Must not have been sanctioned or administratively disciplined in the last two (2) months.
   </li>
   <li>
   Must be dedicated to all aspects and membership of their respective game section.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 4
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
    &nbsp;<img src="images/j_member.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Actively recruits new prospects on any of our public gaming servers.
   </li>
   <li>
   Provide input and feedback on discussions that impact the direction of the game section that they are apart of.
   </li>
   <li>
   Provide leadership and direction on the forums, voice servers, and in game to all Prospects and potential members.
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Must have maintained the key qualities as noted above throughout their progression to becoming a Junior Member.
   </li>
   <li>
   Requires a total of 6 to 8 Weeks in Exiled Order.
   </li>
   <li>
   Must not have any active sanctions or probationary charges.
   </li>
   <li>
   Must be active in the community on the forums, voice servers, and in game.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 3
   </li>
   </ul>
   <hr size="1px;" color="#ba9c22"><br/>
   &nbsp;<img src="images/prospect.png" /><br/>
    <h1>Responsibilities:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Non-Applicable
   </li>
   </ul>
    <h1>Requirements:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Complete the Application Process of Exiled Order.
   </li>
   <li>
   Remain active within their respective game section and within the community.
   </li>
   <li>
   Must be active on forums, game servers, and voice servers.
   </li>
   </ul>
   <h1>Security Clearance:</h1>
   <ul style="list-style-image: url('images/small_icon.png');">
   <li>
   Level: 3
   </li>
   </ul>
   
   
</div>
<center><img src="images/structure.png" /></center>
</div>

