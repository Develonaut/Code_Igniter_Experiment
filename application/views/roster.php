<div style="height: 100px;"></div>
<img src="images/roster_banner.png" alt="roster"/>
<div id="roster_disp">
	<center><h1>LEADERS & FOUNDERS</h1></center>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="forums/index.php?/profile/1-delly/" class="roster">Delly</a></div></div>
        <div class="roster_rank"><div class="roster_text">Leader&nbsp;/&nbsp;Founder</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            <img src="images/game_cs.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/30/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="forums/index.php?/profile/4-lokil/" class="roster">Lokil</a></div></div>
        <div class="roster_rank"><div class="roster_text">Leader&nbsp;/&nbsp;Founder</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            <img src="images/game_cs.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/30/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="mloa">MLOA</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="forums/index.php?/profile/3-skippy/" class="roster">Skippy</a></div></div>
        <div class="roster_rank"><div class="roster_text">Leader&nbsp;/&nbsp;Founder</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            <img src="images/game_cs.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/30/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <center><h1>OPERATIONS</h1></center>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="forums/index.php?/profile/2-yarbird/" class="roster">Yarbird</a></div></div>
        <div class="roster_rank"><div class="roster_text">Head Administrator</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            <img src="images/game_cs.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/1/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="forums/index.php?/profile/5-lynch/" class="roster">Lynch</a></div></div>
        <div class="roster_rank"><div class="roster_text">Administrator</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />			
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/18/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="forums/index.php?/profile/6-shnan/" class="roster">Shnan</a></div></div>
        <div class="roster_rank"><div class="roster_text">Administrator</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/nz.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_wow.png" alt="game" />
			<img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/30/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
	<center><h1>LOGISTICS SECTOR</h1></center>
	<div class="roster_wrap">
		<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/10-drills/" class="roster">Drills</a></div></div>
        <div class="roster_rank"><div class="roster_text">Foreman</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/ca.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            <img src="images/game_dota.png" alt="game" />
            <img src="images/game_gw2.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/08/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
	    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/8-paperbags/" class="roster">Paperbags</a></div></div>
        <div class="roster_rank"><div class="roster_text">Architect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/30/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/22-christopher-chance" class="roster">Christopher Chance</a></div></div>
        <div class="roster_rank">
          <div class="roster_text">Secretary</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/08/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
	<center><h1>LEGAL DEPARTMENT</h1></center>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/9-dimmie/" class="roster">Dimmie</a></div></div>
        <div class="roster_rank"><div class="roster_text">Military Police / Web Dev</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">11/29/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <center><h1>GAME SECTION MEMBERS</h1></center>
     <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/44-chaos" class="roster">Chaos</a></div></div>
        <div class="roster_rank"><div class="roster_text">Game Leader</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/14/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/17-winchester/" class="roster">Winchester</a></div></div>
        <div class="roster_rank"><div class="roster_text">Game Co-Leader</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/15/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/7-matacular/" class="roster">Matacular</a></div></div>
        <div class="roster_rank"><div class="roster_text">Senior Member 2nd Grade</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">10/30/2014</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="#" class="roster">Ravage0351</a></div></div>
        <div class="roster_rank">
          <div class="roster_text">Member 2nd Grade</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">11/24/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="#" class="roster">MrRecon</a></div></div>
        <div class="roster_rank"><div class="roster_text">Junior Member</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">11/24/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="#" class="roster">neoX</a></div></div>
        <div class="roster_rank"><div class="roster_text">Junior Member</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/nl.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">11/29/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
     <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/13-zouloum/" class="roster">Zouloum</a></div></div>
        <div class="roster_rank">
          <div class="roster_text">Junior Member</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/ca.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">11/29/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
   <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/11-keg" class="roster">Keg</a></div></div>
        <div class="roster_rank">
          <div class="roster_text">Junior Member</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/04/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/12-kellz" class="roster">Kellz</a></div></div>
        <div class="roster_rank">
          <div class="roster_text">Junior Member</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/06/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="mloa">MLOA</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/16-dominguez-æ3465" class="roster">Dominguez</a></div></div>
        <div class="roster_rank"><div class="roster_text">Junior Member</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/10/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/" class="roster">gpellis87</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">11/29/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/30-onikenshin" class="roster">Onikenshin</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">11/30/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/27-afrocano" class="roster">Afrocano</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/06/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/97-digit" class="roster">Digit</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/ca.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/12/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/76-ocsoldier" class="roster">OCsoldier</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/13/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="mloa">MLOA</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/24-roryr6" class="roster">rory6</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/gb.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/13/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/21-zumrok" class="roster">Zumrok</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/se.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/14/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/19-magics" class="roster">Magics</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/15/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/" class="roster">Geomatrix86</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/15/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/14-reneko" class="roster">Reneko</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/gb.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/17/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/23-arktic" class="roster">Arktic</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>    
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/34-noodlified" class="roster">Noodlified</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/fi.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>        
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/18-papakraken" class="roster">PapaKraken</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/32-scooter624" class="roster">Scooter624</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/ca.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div> 
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/35-plague" class="roster">Plague</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div> 
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/31-htex" class="roster">HTEX</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/gb.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div> 	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/20-konniption" class="roster">Konniption</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            <img src="images/game_wow.png" alt="game" />			
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/39-battle" class="roster">Ba[Tt]le</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">		
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>		
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/37-develonaut" class="roster">Develonaut</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/19/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/25-sno" class="roster">Sno</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/21/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/38-svett" class="roster">Svett</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/no.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/21/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/36-le_n00bly" class="roster">Le_n00bly</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/21/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/41-ymmur" class="roster">Ymmur</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/22/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>		
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/51-sparky" class="roster">Sparky</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/22/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/74-spider" class="roster">Spider</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/24/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/77-flyntlawk" class="roster">Flyntlawk</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/26/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/79-combatratx" class="roster">Combatratx</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/26/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/86-dead_rabbit" class="roster">Dead_Rabbit</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/28/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/90-fluffhead" class="roster">Fluffhead</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/28/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/94-devotie" class="roster">DeVotie</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/29/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/98-menace" class="roster">Menace</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">12/31/2015</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/93-blaze" class="roster">Blaze</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/us.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">1/1/2016</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>		
    <div class="roster_wrap">
    	<div class="roster_icon"><div class="roster_text"><img src="images/small_icon.png" alt="icon"/></div></div>
    	<div class="roster_name"><div class="roster_text"><a href="http://exiledorder.com/forums/index.php?/profile/82-biggyg" class="roster">Biggy^G</a></div></div>
        <div class="roster_rank"><div class="roster_text">Prospect</div></div>
		<div class="roster_flag"><div class="roster_text_flag"><img src="images/flag/ca.png" alt="icon"/></div></div>
        <div class="roster_game">
            <div class="roster_text">
            <img src="images/game_squad.png" alt="game" />
            </div>
        </div>
        <div class="roster_date"><div class="roster_text">1/1/2016</div></div>
        <div class="roster_status"><div class="roster_text"><span class="active">Active</span></div></div>
    </div>
    <div style="clear: both;"></div>	
     <div class="roster_wrap"> 
        <div class="roster_rank"><div class="roster_text">Total Members: 53</div></div>

		


		
</div>
